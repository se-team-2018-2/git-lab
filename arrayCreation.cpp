#include <ctime>
#include <cstdlib>

int *arrayCreation(const int size, const int rangeMax, const int rangeMin) {

	int *array = new int[size];

	srand(time(0));

	for (int i = 0; i < size; i++) {

		array[i] = rand() % (rangeMax + rangeMin + 1) + rangeMin;
	}

	return array;
}