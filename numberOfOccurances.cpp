#include "FrequencyOfNumber.cpp"

void numberOfOccurrences(int *array, const int size, FrequencyOfNumber*& res, int& res_len) {

	int quantity = 0;
	int count = 1;

	for (int i = 0; i < size - 1; i++) {

		for (int j = i + 1; j < size; j++) {

			if (array[i] == array[j]) {

				break;
			}

			if (array[i] != array[j]) {

				count++;
				break;
			}
		}
	}

	FrequencyOfNumber *numbersWithQuantity = new FrequencyOfNumber[count];

	int currentCount = 0;

	for (int i = 0; i < size; i++) {

		bool flag = true;

		for (int j = 0;j < count;j++) {

			if (array[i] == numbersWithQuantity[j].number) {

				numbersWithQuantity[j].count++;
				flag = false;
				break;
			}
		}

		if (flag) {

			numbersWithQuantity[currentCount++].number = array[i];
		}
	}

	res = numbersWithQuantity;
	res_len = count;
}
