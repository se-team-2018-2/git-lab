int findLastMaximum(int *array, const int size) {

	int maximumPosition = 0;
	int maximum = array[0];

	for (int i = 1; i < size; i++) {

		if (array[i] >= maximum) {

			maximum = array[i];
			maximumPosition = i;
		}
	}

	return maximumPosition;
}