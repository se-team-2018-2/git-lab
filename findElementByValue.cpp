int findElementByValue(int *array, const int size, const int chosenElement) {

	for (int i = 0; i < size; i++) {

		if (array[i] == chosenElement) {

			return i;
		}
	}

	return -1;
}