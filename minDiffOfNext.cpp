int abs(int x) {
    return x > 0 ? x : -x;
}

int minDiffOfNext(int* arr, int len) {
    int minDiff = abs(arr[0] - arr[1]);

    for(int i = 1; i < len - 1; i++) {
        int diff = abs(arr[i] - arr[i + 1]);

        if(diff < minDiff)
            minDiff = diff;
    }

    return minDiff;
}