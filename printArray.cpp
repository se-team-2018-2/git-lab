#include <iostream>
#include "FrequencyOfNumber.cpp"

void printArray(int* array, int length) {
	for (int i = 0; i < length; i++)
		std::cout << array[i] << ' ';

	std::cout << std::endl;
}

void printArray(FrequencyOfNumber* array, int length) {
	for (int i = 0; i < length; i++)
		std::cout << array[i].number << ": " << array[i].count << std::endl;
}
