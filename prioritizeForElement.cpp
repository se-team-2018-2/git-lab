#include <algorithm>
#include "FrequencyOfNumber.cpp"

bool compare(FrequencyOfNumber a, FrequencyOfNumber b) {
    return a.count < b.count;
}

int* prioritizeForElement(FrequencyOfNumber* numbers, int length, int numberCount) {
    std::sort(numbers, numbers + length - 1, compare);

    int resLen = 0;
    int* array = new int[numberCount];

    for(int i = 0; i < length; i++) {
        for(int j = 0; i < numbers[i].count; j++)
            array[resLen++] = numbers[i].number;
    }

    return array;
}