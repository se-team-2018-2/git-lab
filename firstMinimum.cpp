int firstMinimum(int* arr, int len) {
    int minI = 0;
    int minVal = arr[0];

    for(int i = 1; i < len; i++)
        if(arr[i] < minVal) {
            minVal = arr[i];
            minI = i;
        }

    return minI;
}