void allocatingSubarray(int* array, int length, int*& res, int& res_len) {
	res_len = 0;
	int max_i;
	int Max = 0;

	for(int i = 0; i < length; i++) {

		if(array[i] == 0) {
			while(array[i+1] != 0){
				res_len++;
			}
		}

		if (res_len > Max) {
			Max = res_len;
			max_i = i;
		}
	}

	res = new int[res_len];

	for (int i = 0; i < res_len; i++){ 
		res [i] = array[max_i];
		max_i++; 
	}
}