int* copyWithFill(int* source, int len, int begin, int end, int value) {
    int* arr = new int[len];

    for(int i = 0; i < begin; i++)
        arr[i] = source[i];

    for(int i = begin; i <= end; i++)
        arr[i] = value;

    for(int i = end + 1; i < len; i++)
        arr[i] = source[i];
    
    return arr;
}