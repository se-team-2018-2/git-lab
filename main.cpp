#include <ctime>
#include <cstdlib>
#include <algorithm>
#include <iostream>

int *arrayCreation(const int size, const int rangeMax, const int rangeMin) {

	int *array = new int[size];

	srand(time(0));

	for (int i = 0; i < size; i++) {

		array[i] = rand() % (rangeMax - rangeMin + 1) + rangeMin;
	}

	return array;
}

struct FrequencyOfNumber {

	int number;
	unsigned short count;

    FrequencyOfNumber() {
        count = 1;
    }

};

void numberOfOccurrences(int *array, const int size, FrequencyOfNumber*& res, int& res_len) {

	int count = 0;

	for (int i = 0; i < size; i++) {
		bool is_unique = true;

        for(int j = 0; j < i; j++)
            if(array[i] == array[j]) {
                is_unique = false;
                break;
            }

        if(is_unique)
            count++;
	}

	FrequencyOfNumber *numbersWithQuantity = new FrequencyOfNumber[count];

	int currentCount = 0;

	for (int i = 0; i < size; i++) {

		bool flag = true;

		for (int j = 0;j < count;j++) {

			if (array[i] == numbersWithQuantity[j].number) {

				numbersWithQuantity[j].count++;
				flag = false;
				break;
			}
		}

		if (flag) {

			numbersWithQuantity[currentCount++].number = array[i];
		}
	}

	res = numbersWithQuantity;
	res_len = count;
}

int findAverage(int* array, int length) {
	int sum = 0;

	for (int i = 0; i < length; i++)
		sum += array[i];

	return sum / length;
}

int firstMinimum(int* arr, int len) {
    int minI = 0;
    int minVal = arr[0];

    for(int i = 1; i < len; i++)
        if(arr[i] < minVal) {
            minVal = arr[i];
            minI = i;
        }

    return minI;
}

int findLastMaximum(int *array, const int size) {

	int maximumPosition = 0;
	int maximum = array[0];

	for (int i = 1; i < size; i++) {

		if (array[i] >= maximum) {

			maximum = array[i];
			maximumPosition = i;
		}
	}

	return maximumPosition;
}

int abs(int x) {
    return x > 0 ? x : -x;
}

int minDiffOfNext(int* arr, int len) {
    int minDiff = abs(arr[0] - arr[1]);

    for(int i = 1; i < len - 1; i++) {
        int diff = abs(arr[i] - arr[i + 1]);

        if(diff < minDiff)
            minDiff = diff;
    }

    return minDiff;
}

void printArray(int* array, int length) {
	for (int i = 0; i < length; i++)
		std::cout << array[i] << ' ';

	std::cout << std::endl;
}

void printArray(FrequencyOfNumber* array, int length) {
	for (int i = 0; i < length; i++)
		std::cout << array[i].number << ": " << array[i].count << std::endl;
}

int findMaxInEven (int* array, const int length) {
	int Max = array[0];
	for (int i = 1; i < length; i++)
		if(( i % 2 == 0) && (array[i] >= Max))
			Max = array[i];

    return Max;
}

bool compare(FrequencyOfNumber a, FrequencyOfNumber b) {
    return a.count < b.count;
}

int* prioritizeForElement(FrequencyOfNumber* numbers, int length, int numberCount) {
    std::sort(numbers, numbers + length - 1, compare);

    int resLen = 0;
    int* array = new int[numberCount];

    for(int i = 0; i < length; i++) {
        for(int j = 0; j < numbers[i].count; j++)
            array[resLen++] = numbers[i].number;
    }

    return array;
}

void allocatingSubarray(int* array, int length, int* res, int& res_len) {

    int zeroCount = 0;
    for (int i = 0; i < length; i++)
         if(array[i] == 0)
            zeroCount++;

    if (zeroCount < 2)
        return;
    
    int start = 0;
    int maxLen = -1;

    int rBegin, rEnd;

    while(1) {
        int begin;

        for(int i = start; i < length; i++) {
            if(array[i] == 0) {
                begin = i;
                break;
            }
        }

        int end = -1;
        for(int i = begin + 1; i < length; i++) {
            if(array[i] == 0) {
                end = i;
                break;
            }
        }

        if(end == -1)
            break;

        if(end - begin - 1 > maxLen) {
            rBegin = begin;
            rEnd = end;
            maxLen = end - begin;
        }

        start = end;
    }

    for(int i = rBegin + 1, j = 0; i < rEnd; i++, j++) {
        res[j] = array[i];
    }

    res_len = maxLen;
}

int* copyWithFill(int* source, int len, int begin, int end, int value) {
    int* arr = new int[len];

    for(int i = 0; i < begin; i++)
        arr[i] = source[i];

    for(int i = begin; i <= end; i++)
        arr[i] = value;

    for(int i = end + 1; i < len; i++)
        arr[i] = source[i];
    
    return arr;
}

int main() {
    const int N = 30;

    int* array_a = arrayCreation(N, 15, -7);

    printArray(array_a, N);

    int first_min = firstMinimum(array_a, N);
    int last_max = findLastMaximum(array_a, N);

    int avg = first_min < last_max ?
        findAverage(array_a + first_min, last_max - first_min)
        : findAverage(array_a + last_max, first_min - last_max);
    
    std::cout << "Average in range [first_min, last_max] is " << avg << '\n';

    bool diff_less_3 = minDiffOfNext(array_a, N) < 3;

    if(diff_less_3)
        std::cout << "There are elements with diff less than 3\n";
    else
        std::cout << "There are no elements with diff less than 3\n";        

    int max_of_even = findMaxInEven(array_a, N);

    int first_0_i = 0;
    for(int i = 1; i < N; i++)
        if(array_a[i] == 0) {
            first_0_i = i;
            break;
        }

    int* array_b = first_0_i < last_max ?
        copyWithFill(array_a, N, first_0_i, last_max, max_of_even) 
        : copyWithFill(array_a, N, last_max, first_0_i, max_of_even);

    printArray(array_b, N);

    FrequencyOfNumber* freqs;
    int freqs_len;
    numberOfOccurrences(array_b, N, freqs, freqs_len);

    int* array_b_m = prioritizeForElement(freqs, freqs_len, N);

    printArray(freqs, freqs_len);

    int array_c[30];
    int array_c_len = 0;
    allocatingSubarray(array_b, N, array_c, array_c_len);

    printArray(array_c, array_c_len - 1);

    if(array_c_len == 0) {
        std::cout << "Array C is empty\n";
    }

    return 0;
}